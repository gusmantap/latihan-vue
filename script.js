var app = new Vue({
	el:"#app",
	mounted:function(){
		var self = this;
		var url = this.url+'/LogoCtrl/load_logo_favorite';
		var config = {
			headers:{
				'Content-type': 'application/x-www-form-urlencoded'
			}
		};
		axios.post(url, Qs.stringify({id_business:this.id_business})).then(function(res){
			var newData = res.data;
			self.$nextTick(function(){			
				_.forEach(newData, function(val, key){
					if(key === 1){
						val['isActive'] = true;
					} else {
						val['isActive'] = false;
					}
					self.logos.push(val);
				})
			})
		})
	},
	data:{
		canvas:document.querySelector("#editor-logo .body"),
		widthActual:560,
		heightActual:454,
		currentWidth:400,
		name:'ded ded',
		url:document.querySelector('meta[name="url"]').getAttribute('content'),
		id_business:document.querySelector('meta[name="id_business"]').getAttribute('content'),
		logos:[]
	},
	computed:{
		currentHeight: function(){
			return this.heightActual / this.widthActual * this.currentWidth;
		},
		logo_active:function(){
			return _.find(this.logos, ['isActive', true]);
		}
	},
	methods:{
		selectLogo:function(selected){
			// Set all false
			_.forEach(this.logos, function(val, key){
				val.isActive = false;
			});
			selected.isActive = true;
		},
		deleteLogo:function(selected){
			// selected
			var before = selected - 1;
			var after = selected + 1;
			if(before > -1){
				this.selectLogo(this.logos[before]);
			} else if(before === 0) {
				this.selectLogo(this.logos[after]);
			}

			this.logos.splice(selected, 1);
		},
		prevLogo: function(){
			var current = _.findIndex(this.logos, ['isActive', true]);
			
			var prev = current - 1;

			console.log(prev);
			console.log(this.logos[prev]);
			if(prev >= 0){
				this.selectLogo(this.logos[prev]);
			}
		},
		nextLogo: function(){
			var current = _.findIndex(this.logos, ['isActive', true]);
			var next = current + 1;

			if(next < this.logos.length){
				this.selectLogo(this.logos[next]);
			}
		}
	},
	filters:{
		autoEnter:function(val){
			var text = val.split(' ');
			var entered = '';
			_.forEach(text, function(val, key){
				entered += '<br>'+val;
			});

			return entered;
		}
	}

});