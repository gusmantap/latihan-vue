var store = new Vuex.Store({
	state:{
		business:{
			title:{
				line_1:'',
				line_2:''
			},
			tagline:''
		},
		favorite:[],
		history:[],
		active:{
			// disposition:'',
			// icon:{
			// 	id_icon:'',
			// 	icon_file_name:'',
			// 	icon_source:'',
			// 	icon_path:'',
			// 	icon_height:'',
			// 	icon_width:'',
			// 	icon_rotate:'',
			// 	isContour:'',
			// 	contourWidth:''
			// },
			// title:{
			// 	id_font:'',
			// 	font_name:'',
			// 	font_url:'',
			// 	font_source:'',
			// 	font_size:'',
			// 	isBold:'',
			// 	isItalic:''
			// },
			// tagline:{
			// 	id_font:'',
			// 	font_name:'',
			// 	font_url:'',
			// 	font_source:'',
			// 	font_size:'',
			// 	isBold:'',
			// 	isItalic:''	
			// },		
			// illustration:{
			// 	id_illustration:'',
			// 	illustration_file_name:'',
			// 	illustration_path:'',
			// 	illustration_source:'',
			// 	illustration_link:''
			// },
			// color:{
			// 	id_color:'',
			// 	colors:''
			// }
		}
	},
	getters:{
		getActive:function(state, getters){
			return state.active;
		},
		getActiveColor: function(state, getters){
			return state.active.color;
		},
		getIcon: function(state, getters){
			return state.active.icon;
		},
		titleBusiness:function(state, getters){
			return state.business.title;
		},
		taglineBusiness:function(state, getters){
			return state.business.tagline;
		},
		idbusiness: function(state, getters){
			return state.business.idbusiness;
		},
		idcategory: function(state, getters){
			return state.business.idcategory;
		}
	},
	mutations:{
		setActive:function(state, data){
			state.active = data;
		},
		setColor:function(state, data){
			if(data.id == 1){
				state.active.color.colors.color_1 = data.color;
			} else if(data.id == 2){
				state.active.color.colors.color_2 = data.color;
			} else if(data.id == 3){
				state.active.color.colors.color_3 = data.color;
			} else if(data.id == 4){
				state.active.color.colors.color_4 = data.color;
			} else if(data.id == 5){
				state.active.color.colors.color_5 = data.color;
			} else if(data.id == 6){
				state.active.color.colors.color_6 = data.color;
			} else {
				state.active.color = data;			
			}
		},
		setLayoutLogo: function(state, layout){
			return state.active.disposition = layout;
		},
		setDataBusiness:function(state, businessData){			
			state.business = {
				idbusiness:businessData.idbusiness,
				idcategory:businessData.idcategory,
				title:{
					line_1:businessData.title.line_1,
					line_2:businessData.title.line_2
				},
				tagline:businessData.tagline
			};
		},
	}
});