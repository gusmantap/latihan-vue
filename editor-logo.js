// Vue Components Component
Vue.component('icon-object', {
    template: '#icon-template',
    store: store,
    data: function() {
        return {
            //     isDisabled: false,
            //     isLocked: false,
            //     isContour: false,
            //     dimension: {
            //         width: '',
            //         height: '',
            //         x: '',
            //         y: ''
            //     },
        };
    },
    computed: {
        icon: function() {
            return this.$store.getters.getIcon;
        }
    }
});

Vue.component('title-object', {
    template: "#title-template",
    data: function() {
        return {
            isDisabled: false,
            isLocked: false,
            isContour: false,
            dimension: {
                size: '',
                x: '',
                y: ''
            }
        }
    },
    props: ['title', 'font_family']
});

var editor = Vue.component('tagline-object', {
    template: "#tagline-template",
    data: function() {
        return {
            isDisabled: false,
            isLocked: false,
            isContour: false,
            dimension: {
                size: '',
                x: '',
                y: ''
            }
        }
    },
    props: ['font_family']
});


Vue.component('editor-logo', {
    template: '#editor-logo-template',
    // data:function(){
    //  return {
    //      // layout:'bottom'
    //  }
    // },
    store: store,
    computed: {
        title: function() {
            return this.$store.getters.titleBusiness;
        },
        tagline: function() {
            return this.$store.getters.taglineBusiness;
        },
        active:function(){
            return this.$store.getters.getActive;
        }
    },
    props: ['layout', 'is-loading']
});


Vue.component('color-main', {
    template: '#color-main-template',
    store: store,
    computed: {
        color: function() {
            return colordata = this.$store.getters.getActiveColor;
        }
    },
    methods: {
        // setColor:function(){
        //  var _this = this;
        //  $("#color_1").spectrum({
        //      color:_this.color.color_1
        //  });
        // }
    }
});

Vue.component('color-picker', {
    template: '#color-picker-template',
    store: store,
    props: ['id', 'color'],
    mounted: function() {
        var _this = this;
        var input = $(_this.$el).find("input");
        input.spectrum();

        input.on('change.spectrum', function(e, color) {
            _this.$store.commit('setColor', {
                'id': _this.id,
                'color': color.toHexString()
            });
        });
    },
    watch: {
        color: function(newVal, oldVal) {
            var _this = this;
            $(_this.$el).find("input").spectrum("set", newVal);
            this.$store.commit('setColor', { id: _this.id, color: newVal });
        }
    },
});

Vue.component('list-favorite', {
    template: '#favorite-list-template',
    store: store,
    data: function() {
        return {
            url: 'http://localhost/artiwiz/',
            logoFavorite: []
        }
    },
    mounted: function() {
        this.loadFavorite();
    },
    computed: {
        idbusiness: function() {
            return this.$store.getters.idbusiness;
        },
        idcategory: function() {
            return this.$store.getters.idcategory;
        }
    },
    methods: {
        loadFavorite: function() {
            var _this = this;
            var url = this.url + 'LogoCtrl/load_logo_favorite';
            axios.post(url, Qs.stringify({ 'id_business': this.idbusiness })).then(function(res) {
                res.data.forEach(function(val, i) {
                    _this.addFavorite(val);
                });
            });
        },
        addFavorite: function(logo) {
            this.logoFavorite.push(logo);
        }
    }
});

Vue.component('item-favorite', {
    template: "#favorite-item-template",
    props: ['logo'],
    methods: {
        showId: function(logo) {
            // console.log(this.$parent.$parent);
            app.setDataActive(logo);
        }
    }
});
if(location.host === 'localhost'){
    var urlOrigin = location.origin+'/artiwiz/';
} else {
    var urlOrigin = 'https://artiwiz.com/'; 
}

var app = new Vue({
    el: "#app",
    component: ['editor-logo'],
    store: store,
    data: {
        url: urlOrigin,
        idbusiness: 33,
        idcategory: 81,
        layout: 'top',
        icons: ['d', 'd'],
        isLoading:false
    },
    created: function() {
        var business = {
            idbusiness: 33,
            idcategory: 81,
            title: {
                line_1: 'Strasbourg',
                line_2: 'Football Club'
            },
            tagline: 'lorem ipsum dolor sit amet'
        }
        this.$store.commit('setDataBusiness', business);


        this.loadRandom();
    },
    computed: {
        title: function() {
            return this.$store.getters.titleBusiness;
        },
        active: function() {
            return this.$store.getters.getActive;
        }
    },
    methods: {
        setDisposition: function(position) {
            this.$store.commit('setLayoutLogo', position);
        },

        loadRandom: function() {
            var _this = this;
            _this.isLoading = true;
            NProgress.start();


            var qs = $.param({
                idcategory: this.idcategory,
                idbusiness: this.idbusiness
            });
            var url = this.url + 'api/logo/random?' + qs;

            axios.get(url).then(function(res) {
                _this.setDataActive(res.data);


                setTimeout(function(){
                    _this.isLoading = false;
                    NProgress.done();
                }, 1000)
            });


        },

        setDataActive: function(res) {
            var logoActive = {
                disposition: 'left',
                icon: {
                    id_icon: res.icon.id_icons,
                    icon_file_name: res.icon.icon_file_name,
                    icon_source: res.icon.icon_source,
                    icon_price: res.icon.icon_price,
                    icon_path: res.icon.icon_path,
                    icon_height: 150,
                    icon_width: null,
                    icon_rotate: 0,
                    isContour: false,
                    contourWidth: 0,
                    isLocked: false
                },
                title: {
                    id_font: res.font_title.id_font,
                    font_name: res.font_title.font_name,
                    font_url: res.font_title.font_url,
                    font_source: res.font_title.font_source,
                    font_size: res.font_title.font_size,
                    isBold: false,
                    isItalic: false,
                    isLocked: false
                },
                tagline: {
                    id_font: res.font_tagline.id_font,
                    font_name: res.font_tagline.font_name,
                    font_url: res.font_tagline.font_url,
                    font_source: res.font_tagline.font_source,
                    font_size: res.font_tagline.font_size,
                    isBold: false,
                    isItalic: false,
                    isLocked: false
                },
                illustration: {
                    id_illustration: res.illustration.id_illustration,
                    illustration_file_name: res.illustration.illustration_file_name,
                    illustration_path: res.illustration.illustration_path,
                    illustration_source: res.illustration.illustration_source
                },
                color: {
                    id_color: res.color.id_color,
                    colors: {
                        color_1: res.color.color_1,
                        color_2: res.color.color_2,
                        color_3: res.color.color_3,
                        color_4: res.color.color_4,
                        color_5: res.color.color_5,
                        color_6: res.color.color_6
                    }
                }
            }

            this.$store.commit('setActive', logoActive);
        },
        getLogoPosition() {
            var canvas = $("#editor-canvas");
            var logo = $("#logo");

            var pos = {
                width: logo.width(),
                height: logo.height(),
                x: logo.offset().left - canvas.offset().left,
                y: logo.offset().top - canvas.offset().top
            }
            return pos;
        }
    }
});