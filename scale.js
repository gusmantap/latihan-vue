
var Scaling = function(){
	this.actualWidth = 560;
	this.actualHeight = 454;
	this.targetWidth = null;
	this.targetHeight = null;

	this.calcHeigth = function(){
		var height = this.actualHeight / this.actualWidth * this.targetWidth;
		this.setTargetHeight(height);
	}
	this.setTargetWidth = function(width){
		this.targetWidth = width;
	}
	this.setTargetHeight = function(height){
		this.targetHeight = height;
	}
}