// Dependency jQuery UI

var setFontSizeTagline = function (newfontsize) {
    var targetWidth = 560;
    var actualWidth = $("#canvas-editor .item").width();
    var scale = targetWidth / actualWidth;

    var endpoint = $("#canvas-editor").attr("data-url") + 'LogoCtrl/setTaglineFontSize';
    var idlogo = $("#canvas-editor .item").attr("data-id-logo");
    var fontsize = $("#canvas-editor .item .business-tagline").css('font-size').slice(0 ,-2);

    var datafontsize = fontsize * scale;

    $.ajax({
        url: endpoint,
        method: 'POST',
        data: { 'id-logo': idlogo, 'new-font-size': datafontsize }
    }).done(function(res){
        setLogoTagline(idlogo);
        $("#canvas-editor .item .business-tagline").attr('data-font-size', datafontsize);
        $(".grid-icon li[data-id-logo='" + idlogo + "']").attr('data-font-tagline-size', datafontsize);
    });
}

var setFontSizeTitle = function (newfontsize) {
    var targetWidth = 560;
    var actualWidth = $("#canvas-editor .item").width();
    var scale = targetWidth / actualWidth;

    var endpoint = $("#canvas-editor").attr("data-url") + 'LogoCtrl/setTitleFontSize';
    var idlogo = $("#canvas-editor .item").attr("data-id-logo");
    var fontsize = $("#canvas-editor .item .business-name").css('font-size').slice(0, -2);

    var datafontsize = fontsize * scale;

    $.ajax({
        url: endpoint,
        method: 'POST',
        data: { 'id-logo': idlogo, 'new-font-size': datafontsize}
    }).done(function(res){
        setLogoTitle(idlogo);
        $("#canvas-editor .item .business-name").attr('data-font-size', datafontsize);
        $(".grid-icon li[data-id-logo='" + idlogo + "'] h2").attr('data-font-size', datafontsize);
    });
}


var setLogoTitle = function(idlogo){
    var widthNormal = 560;
    var widthTarget = $("#logo-favorite .grid-icon li").width();
    var scaleLogoHistory = widthTarget / widthNormal;
    var title = $("#canvas-editor [data-id-logo='" + idlogo + "'] .business-name");
    var datafontsize = title.attr('data-font-size');
    var cssfontsize = title.css('font-size').slice(0 ,-2);

    var h2 = $(".grid-icon li[data-id-logo='" + idlogo + "'] h2");
    h2.css('font-size', cssfontsize * scaleLogoHistory);

}

var setLogoTagline = function(idlogo){
    var widthNormal = 560;
    var widthTarget = $("#logo-favorite .grid-icon li").width();
    var scaleLogoHistory = widthTarget / widthNormal;
    var tagline = $("#canvas-editor [data-id-logo='"+idlogo+"'] .business-tagline");
    var datafontsize = tagline.attr('data-font-size');
    // var cssfontsize = title.css('font-size').slice(0, -2);

    var grid = $(".grid-icon li[data-id-logo='" + idlogo + "']");

    $(".grid-icon li[data-id-logo='"+idlogo+"']").attr('data-font-tagline-size', datafontsize);
}

$.fn.resizableText = function (targetTextResize, type, max_width, min_width) {
    var self = this;
    var widthInit = null;
    var fontSizeInit = null;
    var scaleCanvas = null;
    var scaleFontSize = null;
    this.resizable({
        handles: {
            'sw': $(this).find(".ui-resizable-handle")
        },
        resize: function (e, ui) {
            // Set Font Size
            var maxWidth = ($(this).css('max-width').slice(0, -1) * $("#canvas-editor .item").width()) / 100;
            if(ui.size.width <= maxWidth){
                var newWidth = ui.size.width;
                // Set new Font Size
                scaleFontSize = newWidth / widthInit;
                var newFontSize = ((scaleFontSize * fontSizeInit) - 1);
                self.find(targetTextResize).css('font-size', newFontSize + 'px');

            }
            self.css('height', 'auto');
            self.css('left', '0px');
        },
        start: function (e, ui) {
            $(".business-name, .business-tagline").removeClass("animatedInOut");

            // Get Scale Canvas
            var widthNormal = 560;
            var widthTarget = $("#canvas-editor .item").width();
            scaleCanvas = widthTarget / widthNormal;

            widthInit = parseFloat(self.css('width').slice(0, -2));
            fontSizeInit = parseFloat(self.find(targetTextResize).css("font-size").slice(0, -2));
        },
        stop: function (e, ui) {
            ui.element.css('width', 'auto');

            var datafontsize = self.find(targetTextResize).attr('data-font-size');

            if(type === 'title'){
                setFontSizeTitle(datafontsize);
            } else if(type === 'tagline'){
                setFontSizeTagline(datafontsize);
            }

            $(".business-name, .business-tagline").addClass("animatedInOut");
        }
    });
}
